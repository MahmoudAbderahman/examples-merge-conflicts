require 'net/http'
require 'uri'
require 'openssl'

@file = File.open('output.txt', 'w')
@private_token = 'Y5ytVQjshne2Qb3sGYsv'
@pages_number = 16 # number of pages to get from the server
@ar_id_list = []
@regex_exp = /[A][R][-][0-9]+[0-9]/
@user_name = ##
@password = ##
def fill_gitlab_items_list()
    for page in 0..@pages_number
        gitlab_url = "https://code.autosar.org/api/v4/projects/12/merge_requests?state=merged&per_page=100&page=#{page}&private_token="
        gitlab_url = gitlab_url + @private_token
        uri = URI(gitlab_url)
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        request = Net::HTTP::Get.new(uri.request_uri)
        request["Content-Type"] = "application/json"
        gitlab_response = http.request(request)
        gitlab_data = JSON.parse(gitlab_response.body) if gitlab_response.body
        gitlab_data.each do |item|
            item = item['source_branch']
            if item =~ @regex_exp
                @ar_id_list << item[@regex_exp]
            end
            
        end
    end
end


def _send_jira_request(ar_item)
    
      url = 'https://jira.autosar.org/rest/api/2/issue/'
      url = url + ar_item
      
      puts "Connect to #{url}"
      
      uri = URI(url)

      http = Net::HTTP.new(uri.host, uri.port)

      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE

      request = Net::HTTP::Get.new(uri.request_uri)
      request.basic_auth @user_name, @password
      request["Content-Type"] = "application/json"
      
      jira_response = http.request(request)
      data = JSON.parse(jira_response.body) if jira_response.body
      data
end

fill_gitlab_items_list

@ar_id_list.each do |item|
    @file.puts("AR-Item: " + item)
    @file.puts
    @file.puts("Description: ")
    @file.puts(_send_jira_request(item)['fields']['description'])
    @file.puts
    @file.puts("Proposed Solutions: ")
    @file.puts(_send_jira_request(item)['fields']['customfield_10408'])
    @file.puts('##########################################################################################################################################################################')
end

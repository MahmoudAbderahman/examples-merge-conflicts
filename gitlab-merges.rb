require 'net/http'
require 'uri'
require 'openssl'
require 'json'
@file = File.open('output.txt', 'w')
@private_token = 'Y5ytVQjshne2Qb3sGYsv'
@pages_number = 15 # number of pages to get from the server
@ar_id_list = []
@merges_iid_list = []
@merge_diffs = {}
@regex_exp = /[A][R][-][0-9]+[0-9]/
@user_name = 'mahmoud.abdelrahman@carmeq.com'
@password = 'Jc%zF41l'
@gitlab_url = 'https://code.autosar.org/api/v4/projects/12/merge_requests'
def fill_gitlab_items_list()
    for page in 0..@pages_number
        gitlab_url = @gitlab_url + "?private_token=" + @private_token
        gitlab_url = gitlab_url + "&state=merged&per_page=100&page=#{page}"
        gitlab_data = build_request(gitlab_url)
        gitlab_data.each do |item|
            item_with_branch = item['source_branch']
            if item_with_branch =~ @regex_exp
                @ar_id_list << item_with_branch[@regex_exp]
                #puts item["iid"]
                #@merges_iid_list << item["iid"]
                @merge_diffs[item_with_branch[@regex_exp]] ||= get_merge_diffs(item["iid"])
                #puts "Size is #{@merges_iid_list.size}"
            end
            
        end
    end
end

def build_request(url)
  uri = URI(url)
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true
  http.verify_mode = OpenSSL::SSL::VERIFY_NONE
  request = Net::HTTP::Get.new(uri.request_uri)
  request["Content-Type"] = "application/json"
  gitlab_response = http.request(request)
  gitlab_data = JSON.parse(gitlab_response.body) if gitlab_response.body
  gitlab_data
end


def get_merge_diffs(item)

  
  url = @gitlab_url + "/#{item}" + "?private_token=" + @private_token
  gitlab_data = build_request(url)
  gitlab_data["diff_refs"]
  
end



def _send_jira_request(ar_item)
    
      url = 'https://jira.autosar.org/rest/api/2/issue/'
      url = url + ar_item
      
      puts "Connect to #{url}"
      
      uri = URI(url)

      http = Net::HTTP.new(uri.host, uri.port)

      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE

      request = Net::HTTP::Get.new(uri.request_uri)
      request.basic_auth @user_name, @password
      request["Content-Type"] = "application/json"
      
      jira_response = http.request(request)
      data = JSON.parse(jira_response.body) if jira_response.body
      data['fields']['description']
end
fill_gitlab_items_list
#get_merge_diffs
puts @merge_diffs

@ar_id_list.each do |item|
    @file.puts("AR-Item: " + item)
    @file.puts
    @file.puts("Description: ")
    @file.puts(_send_jira_request(item))
    @file.puts
    @file.puts("Diffs: ")
    @file.puts(@merge_diffs[item])
    @file.puts('##########################################################################################################################################################################')
end
